package patterns;

import com.sun.org.apache.xpath.internal.operations.Or;
import model.WMS;
import model.WMSsInterface;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UnmodifiableWMS implements WMSsInterface
{
    private String order;
    public List<WMS> myList;

    public UnmodifiableWMS(String order)
    {
        this.order = order;
    }

    public boolean voegToe(WMS wms)
    {
        throw new UnsupportedOperationException();
    }

    public boolean verwijder(String Order)
    {
        throw new UnsupportedOperationException();
    }

    public WMS zoek(String Order)
    {
        return null;
    }

    public List<WMS> gesorteerdOpMedewerker()
    {
        return Collections.unmodifiableList(myList);
    }
}
