package data;

import model.WMS;
import model.WMSs;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Data
{
    public static List<WMS> getData()
    {
        WMS[] myArray =
                {
                        new WMS("Jason R. Hudgins", "1", "19", "Wortelen", 8, LocalDate.of(2019, 5, 25), true, 3.21),
                        new WMS("Jan De Weveer", "2", "10", "Appelen", 9, LocalDate.of(2019, 3, 25), false, 5.21),
                        new WMS("Bertrand", "3", "11", "peren", 5, LocalDate.of(2019, 3, 25), true, 15.21),
                        new WMS("Bert", "4", "4", "zadels", 5, LocalDate.of(2019, 3, 25), true, 5.21),
                        new WMS("Appelmans", "5", "5", "vlees", 5, LocalDate.of(2019, 5, 25), true, 15.21),
                        new WMS("Kurt", "6", "6", "Zuivel", 4, LocalDate.of(2019, 3, 25), false, 15.21),
                        new WMS("Gert", "7", "3", "Deeg", 5, LocalDate.of(2019, 3, 25), true, 15.21),
                        new WMS("Stefan", "8", "7", "Kersen", 5, LocalDate.of(2019, 6, 25), true, 85.21),
                        new WMS("Mark", "9", "8", "Tomaten", 3, LocalDate.of(2019, 3, 25), false, 5.21),
                        new WMS("Stijn", "10", "13", "Asperges", 15, LocalDate.of(2019, 8, 25), true, 6.21),
                        new WMS("Ali", "11", "14", "Kiwi's", 5, LocalDate.of(2019, 3, 25), true, 15.21),
                        new WMS("Jorgen", "12", "8", "Groenten", 7, LocalDate.of(2019, 3, 25), false, 15.21),
                        new WMS("Tom", "13", "7", "Wortelen", 15, LocalDate.of(2019, 9, 25), false, 8.21),
                        new WMS("Tim", "14", "3", "Wortelen", 17, LocalDate.of(2019, 4, 25), true, 15.21),
                        new WMS("Robin", "15", "15", "Wortelen", 18, LocalDate.of(2019, 3, 25), false, 15.21),
                        new WMS("Jason", "16", "12", "Wortelen", 19, LocalDate.of(2019, 8, 25), true, 15.21)

                };

        return new ArrayList<>(Arrays.asList(myArray));
    }
}
