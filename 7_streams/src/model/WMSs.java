package model;

import data.Data;

import java.util.*;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

public class WMSs
{
    private Set<WMS> wmsSet = new TreeSet<>(Data.getData());

    public boolean voegToe(WMS wms)
    {
        return wmsSet.add(wms);
    }

    public boolean verwijder(String Order)
    {
        for (Iterator<WMS> iterator = wmsSet.iterator(); iterator.hasNext();)
        {
            WMS next = iterator.next();
            if(next.getOrder().equals(Order))
            {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public WMS zoek(String Order)
    {
        for(WMS order: wmsSet)
        {
            if(order.getOrder().equals(Order))
            {
                return order;
            }
        }
        return null;
    }

    public List<WMS> gesorteerdOpMedewerker()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList);
        return myList;
    }

    public List<WMS> gesorteerdOp(Function<WMS, Comparable> f)
    {
        List<WMS> lijst = new ArrayList(this.wmsSet);
        Collections.sort(lijst, Comparator.comparing(f));
        return lijst;
    }




}


