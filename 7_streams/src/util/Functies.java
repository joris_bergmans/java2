package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

public class Functies
{
    public static <T> List<T> filteredList(List<T> wmsList, Predicate<T> predicate)
    {
        List<T> list = new ArrayList<>();
        for(T entry: wmsList)
        {
            if(predicate.test(entry))
            {
                list.add(entry);
            }
        }
        return list;
    }

    public static <T> Double averageCollection (List<T> wmsList, ToDoubleFunction<T> mapper)
    {
        double sum = 0;

        /*if(!wmsList.isEmpty())
        {
            for(T entry : wmsList)
            {
                sum = sum + mapper.applyAsDouble(entry);

            }
        }
        sum = sum / wmsList.size();
        System.out.println(sum);*/
        sum = wmsList.stream().mapToDouble(x -> mapper.applyAsDouble(x)).sum();
        sum = sum / wmsList.size();
        System.out.println(sum);

        return sum;
    }

    public static <T> long countIf(Collection<T> wmsList, Predicate<T> predicate)
    {
        long sum = 0;
        for(T entry: wmsList)
        {
            if(predicate.test(entry))
            {
                sum = wmsList.stream().count();
            }
        }
        return sum;
    }
}
