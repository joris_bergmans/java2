import data.Data;
import model.WMS;
import model.WMSs;
import util.Functies;

import java.util.ArrayList;
import java.util.List;

public class Demo_7
{
    public static void main(String args[])
    {
        /*WMSs wmSs = new WMSs();
        System.out.println("\nDictators gesorteerd op naam:");
        for (WMS wms : wmSs.gesorteerdOp(WMS::getMedewerker))
        {
            System.out.println(wms);
        }*/


       /*List<WMS> wmsList = new ArrayList(Data.getData());
       Functies.filteredList(wmsList, wms -> wms.getPallet() == "6" ).stream().forEach(System.out::println);*/

       List<WMS> wmsList = Data.getData();
       Functies.averageCollection(wmsList, x -> x.getMagazijnregio());
    }
}
