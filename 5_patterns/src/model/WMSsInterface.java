package model;

import java.util.List;

public interface WMSsInterface {
    boolean voegToe(WMS wms);

    boolean verwijder(String Order);

    WMS zoek(String Order);

    List<WMS> gesorteerdOpMedewerker();
}
