package model;

import java.time.LocalDate;

public class WMS implements Comparable<WMS>
{
    private String medewerker;
    private String order;
    private String pallet;
    private String product;
    private int magazijnregio;
    private LocalDate productiedatum;
    private boolean pickingafgerond;
    private double gewicht;

    public WMS()
    {
        this.medewerker = "onbekend";
        this.order = "TestOrder";
        this.pallet = "TestPallet";
        this.product = "TestProduct";
        this.magazijnregio = 5;
        this.productiedatum = LocalDate.of(2015, 3,25);
        this.pickingafgerond = true;
        this.gewicht = 15.23;
    }

    public WMS(String medewerker, String order, String pallet, String product, int magazijnregio, LocalDate productiedatum, boolean pickingafgerond, double gewicht)
    {
        this.medewerker = medewerker;
        this.order = order;
        this.pallet = pallet;
        this.product = product;
        this.magazijnregio = magazijnregio;
        this.productiedatum = productiedatum;
        this.pickingafgerond = pickingafgerond;
        this.gewicht = gewicht;
    }

    public String getMedewerker() {
        return medewerker;
    }

    public String getOrder() {
        return order;
    }

    public String getPallet() {
        return pallet;
    }

    public String getProduct() {
        return product;
    }

    public int getMagazijnregio() {
        return magazijnregio;
    }

    public LocalDate getProductiedatum() {
        return productiedatum;
    }

    public boolean isPickingafgerond() {
        return pickingafgerond;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setMedewerker(String medewerker)
    {
        if(medewerker.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een medewerker zijn ingevuld!");
        }
        this.medewerker = medewerker;
    }

    public void setOrder(String order)
    {
        if(order.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een order zijn ingevuld!");
        }
        this.order = order;
    }

    public void setPallet(String pallet)
    {
        if(pallet.isEmpty())
        {
            throw new IllegalArgumentException("Pallet mag niet leeg zijn!");
        }
        this.pallet = pallet;
    }

    public void setProduct(String product)
    {
        if(product.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een product zijn ingevuld!");
        }
        this.product = product;
    }

    public void setMagazijnregio(int magazijnregio)
    {
        if(magazijnregio < 0)
        {
            throw new IllegalArgumentException("Magzijnregio moet een geldige nummer hebben!");
        }
        this.magazijnregio = magazijnregio;
    }

    public void setProductiedatum(LocalDate productiedatum)
    {
        if(productiedatum.isAfter(LocalDate.now()))
        {
            throw new IllegalArgumentException("Productiedatum kan niet na vandaag zijn!");
        }
        this.productiedatum = productiedatum;
    }

    public void setPickingafgerond(boolean pickingafgerond)
    {
        if(pickingafgerond == false)
        {
            throw new IllegalArgumentException("De picking is nog niet afgerond, gelieve eerst alles in te vullen alvorens te verwerken!");
        }
        this.pickingafgerond = pickingafgerond;
    }

    public void setGewicht(double gewicht)
    {
        if(gewicht <= 0)
        {
            throw new IllegalArgumentException("Gewicht mag niet onder de 0 komen of gelijk zijn aan 0, gelieve een geldig gewicht in te vullen");
        }
        this.gewicht = gewicht;
    }

    @Override
    public int compareTo(WMS o)
    {
        return this.magazijnregio - o.magazijnregio;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if(o == null || getClass() != o.getClass())
        {
            return false;
        }
        WMS wms = (WMS) o;
        return order == wms.order;
    }

    @Override
    public int hashCode()
    {
        return magazijnregio;
    }

    @Override
    public String toString()
    {
        return "Person{" + "Order='" + order + '\'' + ", Pallet='" + pallet + '\'' + ", Product='" + product + '\'' + ", Magazijnregio='" + magazijnregio + '\'' + '}';
    }

}
