package patterns;

import model.WMS;
import model.WMSs;
import model.WMSsInterface;

import java.util.List;
import java.util.Observable;

public class ObservableWMS extends Observable implements WMSsInterface
{
    private String order;
    private WMSs wmss = new WMSs();
    public  ObservableWMS(WMSs wms)
    {
        this.wmss = wms;
    }

    @Override
    public boolean voegToe(WMS wms)
    {
        boolean result = wmss.voegToe(wms);
        if(result)
        {
            setChanged();
            notifyObservers("Observer says: Added " + wms.getMedewerker());
        }
        return result;
    }

    @Override
    public boolean verwijder(String Order)
    {

        boolean result = wmss.verwijder(Order);
        if(result)
        {
            setChanged();
            notifyObservers("Observer says: Removed " + Order);
        }
        return result;
    }

    @Override
    public WMS zoek(String Order)
    {
        return null;
    }

    @Override
    public List<WMS> gesorteerdOpMedewerker()
    {
        return null;
    }
}
