package patterns;

import java.util.Observable;
import java.util.Observer;

public class WMSObserver implements Observer
{
    private ObservableWMS wms;

    public WMSObserver()
    {
    }
    @Override
    public void update(Observable o, Object arg)
    {
        System.out.printf("Something has changed, received: %s%n", arg);
    }
}
