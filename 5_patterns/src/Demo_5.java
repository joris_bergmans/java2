import model.WMS;
import model.WMSFactory;
import model.WMSs;
import model.WMSsInterface;
import patterns.ObservableWMS;
import patterns.WMSObserver;

import java.time.LocalDate;

public class Demo_5
{
    public static void main(String args[])
    {
        /*WMSs wms = new WMSs();
        ObservableWMS o = new ObservableWMS(wms);
        WMSObserver w = new WMSObserver();
        o.addObserver(w);

        o.voegToe(new WMS("medewerker", "1234", "pallet", "product", 0, LocalDate.now(), true, 5.5));
        o.verwijder("1234");*/

        /*WMSsInterface wms =  new UnmodifiableWMS("1344");
        System.out.println("Main ontvangt: " + wms.verwijder("1234"));*/

        for (int i = 0; i < 6; i++)
        {
            System.out.println(WMSFactory.newRandomWMS().toString());
        }
    }
}
