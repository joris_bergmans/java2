import Persist.WMSsdbDao;
import data.Data;
import model.WMS;
import model.WMSs;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Demo_8
{
    public static void main(String args[])
    {
        /*WMSs wmSs = new WMSs(Data.getData());

        System.out.println("Controleafdruk:");
        System.out.println(wmSs.wmsList);

        try ( FileOutputStream fileOut = new FileOutputStream("8_persistentie/db/wmss.ser"); ObjectOutputStream out = new ObjectOutputStream(fileOut))
        {
            out.writeObject(wmSs);
            System.out.println("wmsList-object opgeslagen in 'wms.ser'");
            System.out.println(wmSs);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

        try (FileInputStream fileIn = new FileInputStream("8_persistentie/db/wmss.ser"); ObjectInputStream in = new ObjectInputStream(fileIn))
        {
            WMSs list = (WMSs) in.readObject();
            System.out.println("fmdqs");
            System.out.println("order:");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/

        WMSsdbDao wmSsdbDao = new WMSsdbDao("C:\\Users\\joris\\IdeaProjects\\GroeiProject\\8_persistentie\\db");
        try
        {
            for (WMS wms : Data.getData())
            {
                wmSsdbDao.create(wms);
            }
        }
        finally {
            wmSsdbDao.close();
        }
    }
}
