package model;

import Persist.WMSsDAO;

import java.io.Serializable;
import java.util.*;

public class WMSs implements Serializable, WMSsDAO {
    public List<WMS> wmsList;
    public WMSs(List<WMS> List)
    {
        this.wmsList = List;
    }
    public Set<WMS> wmsSet = new TreeSet<>();

    @Override
    public boolean voegToe(WMS wms)
    {
        return wmsSet.add(wms);
    }

    @Override
    public boolean verwijder(String Order)
    {
        for (Iterator<WMS> iterator = wmsSet.iterator(); iterator.hasNext();)
        {
            WMS next = iterator.next();
            if(next.getOrder().equals(Order))
            {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public WMS zoek(String Order)
    {
        for(WMS order: wmsSet)
        {
            if(order.getOrder().equals(Order))
            {
                return order;
            }
        }
        return null;
    }

    @Override
    public List<WMS> gesorteerdOpMedewerker()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList);
        return myList;
    }

    @Override
    public String toString()
    {
       return wmsSet.toString();
    }




}


