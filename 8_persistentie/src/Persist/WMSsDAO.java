package Persist;

import model.WMS;

import java.util.List;

public interface WMSsDAO {
    boolean voegToe(WMS wms);

    boolean verwijder(String Order);

    WMS zoek(String Order);

    List<WMS> gesorteerdOpMedewerker();

    @Override
    String toString();
}
