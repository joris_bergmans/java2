package Persist;

import model.WMS;

import javax.naming.Context;
import javax.naming.NamingException;
import java.sql.*;
import java.util.List;

public class WMSsdbDao implements WMSsDAO
{
    private Connection connection;
    private String path;
    public WMSsdbDao(String path)
    {
        this.path = path;
        try {
           connection = DriverManager.getConnection("jdbc:hsqldb:file:C:\\Users\\joris\\IdeaProjects\\GroeiProject\\8_persistentie\\db","username", "password");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        maakTabel();
    }

    public void close()
    {
        if(path != null)
        {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void maakTabel()
    {
        try
        {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS warehousetable");

            String createQuery = "CREATE TABLE warehousetable (Id INTEGER NOT NULL IDENTITY, " +
                    "medewerker VARCHAR(50)," +
                    "orders VARCHAR(50)," +
                    "pallet VARCHAR(50)," +
                    "product VARCHAR(50)," +
                    "magazijnregio INTEGER," +
                    "productiedatum DATE," +
                    "pickingafgerond BIT," +
                    "gewicht DOUBLE)";
            statement.execute(createQuery);
            System.out.println("Tabel aangemaakt");

        }
        catch (SQLException e)
        {
            System.err.println("Fout bij het maken van tabel : " + e.getMessage());
        }
    }
    public void create(WMS wms) {

            try {

                PreparedStatement prepStatement = connection.prepareStatement(
                        "INSERT INTO warehousetable (id, medewerker, orders, pallet, product, magazijnregio, productiedatum, pickingafgerond, gewicht) " +
                                "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)");
                prepStatement.setString(1, wms.getMedewerker());
                prepStatement.setString(2, wms.getOrder());
                prepStatement.setString(3, wms.getPallet());
                prepStatement.setString(4, wms.getProduct());
                prepStatement.setInt(5, wms.getMagazijnregio());
                prepStatement.setDate(6, Date.valueOf(wms.getProductiedatum()));
                prepStatement.setBoolean(7, wms.isPickingafgerond());
                prepStatement.setDouble(8, wms.getGewicht());
                prepStatement.executeUpdate();
                prepStatement.close();

            } catch (SQLException e) {
                System.err.println("Fout bij create: " + e);
            }

    }
    @Override
    public boolean voegToe(WMS wms) {
        return false;
    }

    @Override
    public boolean verwijder(String Order) {
        return false;
    }

    @Override
    public WMS zoek(String Order) {
        return null;
    }

    @Override
    public List<WMS> gesorteerdOpMedewerker() {
        return null;
    }
}
