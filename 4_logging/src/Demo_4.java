import model.WMS;
import model.WMSs;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogManager;

public class Demo_4
{
    public static void main(String args[]) throws IOException {
        WMSs wms = new WMSs();
        wms.voegToe(new WMS());
        wms.verwijder("TestOrder");
        URL configURL = Demo_4.class.getResource( "/logging.properties" );

        if (configURL != null)
        {
            try (InputStream is = configURL.openStream())
            {
                LogManager.getLogManager(). readConfiguration(is);

            }
            catch (IOException e)
            {
                System.err.println("Configuratiebestand is corrupt");
            }
        }
        else
            {
            System. err.println("Configuratiebestand NIET GEVONDEN" );
        }

    }
}
