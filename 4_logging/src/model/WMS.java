package model;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WMS implements Comparable<WMS>
{
    private String medewerker;
    private String order;
    private String pallet;
    private String product;
    private int magazijnregio;
    private LocalDate productiedatum;
    private boolean pickingafgerond;
    private double gewicht;
    private Logger logger = Logger.getLogger("model.WMS");

    public WMS()
    {
        this.medewerker = "onbekend";
        this.order = "TestOrder";
        this.pallet = "TestPallet";
        this.product = "TestProduct";
        this.magazijnregio = 5;
        this.productiedatum = LocalDate.of(2015, 3,25);
        this.pickingafgerond = true;
        this.gewicht = 15.23;
    }

    public WMS(String medewerker, String order, String s, String wortelen, int i, LocalDate of, boolean b, double v)
    {
        this.medewerker = medewerker;
        this.order = order;
        this.pallet = pallet;
        this.product = product;
        this.magazijnregio = magazijnregio;
        this.productiedatum = productiedatum;
        this.pickingafgerond = pickingafgerond;
        this.gewicht = gewicht;
    }

    public String getMedewerker() {
        return medewerker;
    }

    public String getOrder() {
        return order;
    }

    public String getPallet() {
        return pallet;
    }

    public String getProduct() {
        return product;
    }

    public int getMagazijnregio() {
        return magazijnregio;
    }

    public LocalDate getProductiedatum() {
        return productiedatum;
    }

    public boolean isPickingafgerond() {
        return pickingafgerond;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setMedewerker(String medewerker)
    {
        if(medewerker.isEmpty())
        {
            logger.log(Level.SEVERE, "De gegeven waarde : [0] is leeg" , medewerker);
        }
        this.medewerker = medewerker;
    }

    public void setOrder(String order)
    {
        if(order.isEmpty())
        {
            logger.log(Level.SEVERE, "De gegeven waarde : [0] is leeg" , order);
        }
        this.order = order;
    }

    public void setPallet(String pallet)
    {
        if(pallet.isEmpty())
        {
            logger.log(Level.SEVERE, "De gegeven waarde : [0] is leeg voor " , pallet);
        }
        this.pallet = pallet;
    }

    public void setProduct(String product)
    {
        if(product.isEmpty())
        {
            logger.log(Level.SEVERE, "De gegeven waarde : [0] is leeg" , product);
        }
        this.product = product;
    }

    public void setMagazijnregio(int magazijnregio)
    {
        if(magazijnregio < 0)
        {
            logger.log(Level.SEVERE, "De gegeven waarde : [0] is kleiner dan 0" , magazijnregio);
        }
        this.magazijnregio = magazijnregio;
    }

    public void setProductiedatum(LocalDate productiedatum)
    {
        if(productiedatum.isAfter(LocalDate.now()))
        {
            logger.log(Level.SEVERE, "De productiedatum: [0] ligt na de huidige datum " , productiedatum);
        }
        this.productiedatum = productiedatum;
    }

    public void setPickingafgerond(boolean pickingafgerond)
    {
        if(pickingafgerond == false)
        {
            logger.log(Level.SEVERE, "Picking is nog niet afgerond : [0]", pickingafgerond);
        }
        this.pickingafgerond = pickingafgerond;
    }

    public void setGewicht(double gewicht)
    {
        if(gewicht <= 0)
        {
            logger.log(Level.SEVERE, "De gegeven waarde : [0] is geen geldig gewicht", gewicht);
        }
        this.gewicht = gewicht;
    }

    @Override
    public int compareTo(WMS o)
    {
        return this.magazijnregio - o.magazijnregio;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if(o == null || getClass() != o.getClass())
        {
            return false;
        }
        WMS wms = (WMS) o;
        return this.order == wms.order;
    }

    @Override
    public int hashCode()
    {
        return magazijnregio;
    }

}
