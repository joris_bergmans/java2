package model;

import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

public class WMSs
{
    private Set<WMS> wmsSet = new TreeSet<>();
    Logger logger = Logger.getLogger(WMSs.class.getName());
    FileHandler fileHandler = new FileHandler("mylogging.log", true);


    public WMSs() throws IOException
    {
    }

    public boolean voegToe(WMS wms)
    {
        logger.addHandler(fileHandler);
        logger.log(Level.FINER, wms.toString());
        return wmsSet.add(wms);
    }

    public boolean verwijder(String Order)
    {
        for (Iterator<WMS> iterator = wmsSet.iterator(); iterator.hasNext();)
        {
            WMS next = iterator.next();
            if(next.getOrder().equals(Order))
            {
                logger.addHandler(fileHandler);
                logger.log(Level.FINER, Order);
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public WMS zoek(String Order)
    {
        for(WMS order: wmsSet)
        {
            if(order.getOrder().equals(Order))
            {
                return order;
            }
        }
        return null;
    }

    public List<WMS> gesorteerdOpMedewerker()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList);
        return myList;
    }


}


