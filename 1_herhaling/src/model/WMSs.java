package model;

import java.io.Serializable;
import java.lang.annotation.Inherited;
import java.util.*;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
/**
 * MULTIKLASSE
 * STUDENT: Joris Bergmans
 */
public class WMSs implements Serializable
{
    public Set<WMS> wmsSet = new TreeSet<>();

    public boolean voegToe(WMS wms)
    {
        return wmsSet.add(wms);
    }

    public boolean verwijder(String Order)
    {
        for (Iterator<WMS> iterator = wmsSet.iterator(); iterator.hasNext();)
        {
            WMS next = iterator.next();
            if(next.getOrder().equals(Order))
            {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public WMS zoek(String Order)
    {
        for(WMS order: wmsSet)
        {
            if(order.getOrder().equals(Order))
            {
                return order;
            }
        }
        return null;
    }

    public List<WMS> gesorteerdOpOrder()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList, new OrderComparator());
        return myList;
    }
    public List<WMS> gesorteerdOpPallet()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList, new PalletComparator());
        return myList;
    }
    public List<WMS> gesorteerdOpProduct()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList, new ProductComparator());
        return myList;
    }



    @Override
    public String toString()
    {
        return wmsSet.toString();
    }

    private class OrderComparator implements Comparator<WMS>
    {
        @Override
        public int compare(WMS a, WMS b)
        {
            return a.getOrder().compareToIgnoreCase(b.getOrder());
        }
    }

    private class PalletComparator implements Comparator<WMS>
    {
        @Override
        public int compare(WMS a, WMS b)
        {
            return a.getPallet().compareToIgnoreCase(b.getPallet());
        }
    }
    private class ProductComparator implements Comparator<WMS>
    {
        @Override
        public int compare(WMS a, WMS b)
        {
            return a.getProduct().compareToIgnoreCase(b.getProduct());
        }
    }





}


