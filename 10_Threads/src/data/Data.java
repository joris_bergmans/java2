package data;

import model.WMS;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Data
{
    public static List<WMS> getData()
    {
        WMS[] myArray =
                {
                        new WMS("Jason R. Hudgins", "1", "1", "Wortelen", 5, LocalDate.of(2017, 3, 25), true, 15.21)
                };

        return new ArrayList<>(Arrays.asList(myArray));
    }
}
