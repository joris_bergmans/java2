package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class WMS implements Comparable<WMS>, Serializable
{
    private final String medewerker;
    private final String order;
    private final String pallet;
    private final String product;
    private final int magazijnregio;
    private final LocalDate productiedatum;
    private final boolean pickingafgerond;
    private final double gewicht;

    public WMS()
    {
        this.medewerker = "onbekend";
        this.order = "TestOrder";
        this.pallet = "TestPallet";
        this.product = "TestProduct";
        this.magazijnregio = 5;
        this.productiedatum = LocalDate.of(2015, 3,25);
        this.pickingafgerond = true;
        this.gewicht = 15.23;
    }

    public WMS(String medewerker, String order, String pallet, String product, int magazijnregio, LocalDate productiedatum, boolean pickingafgerond, double gewicht)
    {
        this.medewerker = medewerker;
        this.order = order;
        this.pallet = pallet;
        this.product = product;
        this.magazijnregio = magazijnregio;
        this.productiedatum = productiedatum;
        this.pickingafgerond = pickingafgerond;
        this.gewicht = gewicht;
    }

    public String getMedewerker() {
        return medewerker;
    }

    public String getOrder() {
        return order;
    }

    public String getPallet() {
        return pallet;
    }

    public String getProduct() {
        return product;
    }

    public int getMagazijnregio() {
        return magazijnregio;
    }

    public LocalDate getProductiedatum() {
        return productiedatum;
    }

    public boolean isPickingafgerond() {
        return pickingafgerond;
    }

    public double getGewicht() {
        return gewicht;
    }



    @Override
    public String toString()
    {
        return "Person{" + "Order='" + order + '\'' + ", Pallet='" + pallet + '\'' + ", Product=" + product + '}' + "\n";
    }

    @Override
    public int compareTo(WMS o)
    {
        return this.magazijnregio - o.magazijnregio;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if(o == null || getClass() != o.getClass())
        {
            return false;
        }
        WMS wms = (WMS) o;
        return order == wms.order;
    }

    @Override
    public int hashCode()
    {
        return magazijnregio;
    };

}
