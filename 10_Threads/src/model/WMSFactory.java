package model;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Random;

public class WMSFactory
{
    private void WMSFactory()
    {

    }

    public static WMS newEmptyDictator()
    {
        WMS w = new WMS();
        return w;
    }

    public static WMS newFilledWMS(String medewerker, String order, String pallet, String product, int magazijnregio, LocalDate productiedatum, boolean pickingafgerond, double gewicht)
    {
        return new WMS(medewerker, order, pallet, product, magazijnregio, productiedatum, pickingafgerond, gewicht);
    }

    public static WMS newRandomWMS()
    {
        Random random = new Random();

        int minDay = (int) LocalDate.of(2000, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2019, 1, 1).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        LocalDate resultBirthDate = LocalDate.ofEpochDay(randomDay);

        int minInteger = 10;
        int maxInteger = 50;

        int resultInteger = minInteger + random.nextInt(maxInteger - minInteger);

        double start = 10.0;
        double end = 50.0;
        double randomDouble = new Random().nextDouble();

        double resultDouble = start + (randomDouble * (end - start));

        boolean randomBoolean = new Random().nextBoolean();

        return new WMS(generateString(7, 0, true), generateString(7,0,true), generateString(7,0,true), generateString(7,0,true), resultInteger, resultBirthDate, randomBoolean,  resultDouble);


    }

    private static String generateString(int maxWordLength, int wordCount, boolean camelCase)
    {
        byte[] array = new byte[maxWordLength];
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        if(camelCase) {
            generatedString.toUpperCase();
        }
        else {
            generatedString.toLowerCase();
        }

        return generatedString;
    }
}
