package model;

import model.WMS;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;

import static org.junit.Assert.*;

public class TestWMS
{
    private String order;
    private String product;
    private static final double DELTA = 1e-15;

    @Before
    public void setUp() throws Exception
    {
        testEquals();
    }

    @Test
    public void testEquals()
    {
        WMS w = new WMS();
        boolean r = w.equals(order);
        if(r == false)
        {
            System.out.println("Dit is geen gelijke");
        }
    }

    @Test
    public void testOngeldig()
    {
        WMS w = new WMS();
        w.setPallet("");
    }

    @Test
    public void testGeldig()
    {
        WMS w = new WMS();
        w.setGewicht(0.5);
    }
    @Test
    public void testCompareTo()
    {
        WMS w = new WMS();
        w.compareTo(w);
    }
    @Test
    public void testassertEquals()
    {
        try
        {
            assertEquals(12.00, 14.58, DELTA);
        }
        catch (AssertionError e)
        {
            Assert.assertEquals("de waarde die binnenkomt voldoet niet aan de verwachte waarde", "12.00", e.getMessage());
        }

    }
}