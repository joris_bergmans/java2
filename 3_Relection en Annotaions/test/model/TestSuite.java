package model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestWMS.class, TestWMSs.class})
public class TestSuite
{
}
