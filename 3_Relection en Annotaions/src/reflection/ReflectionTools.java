package reflection;

import org.omg.CORBA.OBJECT_NOT_EXIST;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

public class ReflectionTools
{
    public static void classAnalysis(Class aClass)
    {
        try
        {
            Object object = aClass.newInstance();
            List<Field> privateFields = new ArrayList<>();
            Field [] allFields = aClass.getDeclaredFields();
            System.out.printf("Analyse van de klasse: %s\n", object.getClass().getSimpleName());
            System.out.printf("===================================================== \n");
            System.out.printf("Fully qualified name: %s\n", object.getClass().getName());
             System.out.printf("naam van de superklasse %s:\n", aClass.getSuperclass().getName());
            System.out.printf("Naam van de package: %s\n", aClass.getPackage().getName());
            System.out.printf("%d constructors: %s\n", aClass.getDeclaredConstructors().length, aClass.getName());
            for (Constructor constructor : aClass.getDeclaredConstructors()) {

                Class[] parameters = constructor.getParameterTypes();
                String comma = "";
                for (Class parameter : parameters) {
                    System.out.printf(comma + parameter.getName());
                    comma = ",";
                }
            }
            for (Field field : aClass.getDeclaredFields())
            {
                System.out.printf("attributes: %s\n", field.getName());
            }
            for (Method method : aClass.getDeclaredMethods())
            {
                if(method.getName().startsWith("get"))
                {
                    System.out.printf("getters: %s\n", method.getName());
                }
            }

            for (Method method : aClass.getDeclaredMethods())
            {
                if(method.getName().startsWith("set"))
                {
                    System.out.printf("setters: %s\n", method.getName());
                }
            }



        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static Object runAnnotated (Class aClass)
    {
        Object Object = null;
        try
        {
            Object = aClass.newInstance();
            for (Method method : aClass.getSuperclass().getDeclaredMethods())
            {
                CanRun canRun = method.getAnnotation(CanRun.class);
                System.out.println("Annotation of "+ method + " : " + canRun);
                if (canRun != null)
                {
                    method.invoke(Object,"Dummy");
                }
            }

        }
        catch (IllegalAccessException | InstantiationException | InvocationTargetException iE)
        {
            iE.printStackTrace();
        }
        return Object;
    }
}
