package model;
import  model.WMS;

import java.util.*;

public class WMSs
{
    private TreeSet<WMS> wmsSet = new TreeSet<>();

    public boolean voegToe(WMS wms)
    {
        return wmsSet.add(wms);
    }

    public boolean verwijder(String Order)
    {
        for (Iterator<WMS> iterator = wmsSet.iterator(); iterator.hasNext();)
        {
            WMS next = iterator.next();
            if(next.getOrder().equals(Order))
            {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public WMS zoek(String Order)
    {
        for(WMS order: wmsSet)
        {
            if(order.getOrder().equals(Order))
            {
                return order;
            }
        }
        return null;
    }

    public List<WMS> gesorteerdOpMedewerker()
    {
        List<WMS> myList = new ArrayList<>(wmsSet);
        Collections.sort(myList);
        return myList;
    }
}


