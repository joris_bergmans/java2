package model;

import reflection.CanRun;

import java.time.LocalDate;

public class warehouses
{

    private String medewerker;

    private int magazijnregio;
    private LocalDate productiedatum;
    private boolean pickingafgerond;
    private double gewicht;

    public String getMedewerker() {
        return medewerker;
    }



    public int getMagazijnregio() {
        return magazijnregio;
    }

    public LocalDate getProductiedatum() {
        return productiedatum;
    }

    public boolean isPickingafgerond() {
        return pickingafgerond;
    }

    public double getGewicht() {
        return gewicht;
    }

    @CanRun
    public void setMedewerker(String medewerker)
    {
        if(medewerker.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een medewerker zijn ingevuld!");
        }
        this.medewerker = medewerker;
    }



    public void setMagazijnregio(int magazijnregio)
    {
        if(magazijnregio < 0)
        {
            throw new IllegalArgumentException("Magzijnregio moet een geldige nummer hebben!");
        }
        this.magazijnregio = magazijnregio;
    }

    public void setProductiedatum(LocalDate productiedatum)
    {
        if(productiedatum.isAfter(LocalDate.now()))
        {
            throw new IllegalArgumentException("Productiedatum kan niet na vandaag zijn!");
        }
        this.productiedatum = productiedatum;
    }

    public void setPickingafgerond(boolean pickingafgerond)
    {
        if(pickingafgerond == false)
        {
            throw new IllegalArgumentException("De picking is nog niet afgerond, gelieve eerst alles in te vullen alvorens te verwerken!");
        }
        this.pickingafgerond = pickingafgerond;
    }

    public void setGewicht(double gewicht)
    {
        if(gewicht <= 0)
        {
            throw new IllegalArgumentException("Gewicht mag niet onder de 0 komen of gelijk zijn aan 0, gelieve een geldig gewicht in te vullen");
        }
        this.gewicht = gewicht;
    }
}
