package model;

public class generalisatie extends warehouses {
    private String order;
    private String pallet;
    private String product;

    public generalisatie(String order, String pallet, String product)
    {
        this.order = order;
        this.pallet = pallet;
        this.product = product;
    }

    public generalisatie()
    {

    }

    public String getOrder() {
        return order;
    }

    public String getPallet() {
        return pallet;
    }

    public String getProduct() {
        return product;
    }

    public void setOrder(String order)
    {
        if(order.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een order zijn ingevuld!");
        }
        this.order = order;
    }

    public void setPallet(String pallet)
    {
        if(pallet.isEmpty())
        {
            throw new IllegalArgumentException("Pallet mag niet leeg zijn!");
        }
        this.pallet = pallet;
    }

    public void setProduct(String product)
    {
        if(product.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een product zijn ingevuld!");
        }
        this.product = product;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if(o == null || getClass() != o.getClass())
        {
            return false;
        }
        generalisatie generalisatie = (generalisatie) o;
        return order == generalisatie.order;
    }
}
