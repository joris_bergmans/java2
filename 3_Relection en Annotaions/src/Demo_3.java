import model.WMS;
import model.WMSs;
import model.generalisatie;
import model.warehouses;
import reflection.ReflectionTools;
import reflection.ReflectionTools;

import java.io.PrintStream;

public class Demo_3
{
    public static void main(String args[]) {

        generalisatie g = new generalisatie("test", "test", "test");
        ReflectionTools.classAnalysis(warehouses.class);
        ReflectionTools.classAnalysis(WMS.class);
        ReflectionTools.classAnalysis(WMSs.class);

        System.out.printf("Aangemaakt object door runAnnotated methode van ReflectionTools:");
        PrintStream printf = System.out.printf("%s", ReflectionTools.runAnnotated(WMS.class));

        System.out.println();
    }
}
