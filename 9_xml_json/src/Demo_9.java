import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import data.Data;
import model.WMS;
import model.WMSs;
import parsing.WMSStaxParser;
import parsing.WMSsGsonParser;

import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Demo_9
{
    public static void main(String args[])
    {
        /*WMSs w = new WMSs();
        WMSStaxParser Stax = new WMSStaxParser(w, "9_xml_json/");
        Stax.writeXML();*/


        WMS w = new WMS();
        WMSsGsonParser.writeJson(w, "files/myJsonfile.json");
        WMSsGsonParser.readJson("files/myJsonfile.json");
    }
}
