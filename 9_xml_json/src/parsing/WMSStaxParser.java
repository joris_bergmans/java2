package parsing;

import data.Data;
import model.WMS;
import model.WMSs;

import javax.swing.text.html.HTML;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class WMSStaxParser
{
    WMSs wmss = new WMSs(Data.getData());
    XMLStreamWriter sw;
    int count = 1;

    List<WMS> myList = Arrays.asList(new WMS("Mary Jane", "2", "2", "HK758", 5, LocalDate.of(2017, 3, 5), true, 15.60));
    public WMSStaxParser(WMSs wmss, String XMLFilePathToWrite)
    {
        this.wmss = wmss;
        try
        {
             sw = XMLOutputFactory.newInstance().createXMLStreamWriter(new FileWriter(XMLFilePathToWrite));
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (XMLStreamException ex)
        {
            ex.printStackTrace();
        }
    }

    public void writeXML()
    {
        try
        {

            sw.writeStartDocument();
            sw.writeStartElement("WMS");

            for(WMS wms : myList)
            {
                sw.writeStartElement("WMS");
                sw.writeStartElement("Medewerker");
                sw.writeStartElement("Order");
                sw.writeAttribute("Medewerker", wms.getMedewerker());
                sw.writeAttribute("Order", wms.getOrder());
                sw.writeEndElement();
                sw.writeEndElement();
                sw.writeEndElement();
                sw.writeEndDocument();

            }



            sw.close();


        }
        catch (XMLStreamException ex)
        {
            ex.printStackTrace();
        }
    }

    private void WriteMedewerker(WMS medewerker)
    {
        try
        {
            for(WMS wms : myList)
            {
                sw.writeStartElement("Medewerker");
                sw.writeCharacters(wms.getMedewerker());
                sw.writeEndElement();
            }
        }
        catch (XMLStreamException ex)
        {
            ex.printStackTrace();
        }

    }
}
