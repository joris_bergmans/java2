package parsing;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import data.Data;
import model.WMS;
import model.WMSs;
import com.google.gson.annotations.SerializedName;

import java.io.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class WMSsGsonParser
{
    @SerializedName("medewerker")
    private String medewerker;
    @SerializedName("order")
    private String order;
    @SerializedName("productiedatum")
    private LocalDate productiedatum;

    public static void writeJson(WMS wmSs, String filename)
    {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        String jsonString = gson.toJson(wmSs);

        System.out.println(jsonString);
        try (FileWriter jsonWriter = new FileWriter(filename))
        {
            jsonWriter.write(jsonString);
        }
        catch (FileNotFoundException  e)
        {

        }
        catch (IOException e)
        {

        }
    }

    public static void readJson(String filename)
    {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try (BufferedReader data = new BufferedReader(new FileReader(filename)))
        {
            WMS[] wmsArray = gson.fromJson(data, WMS[].class);
            List<WMS> otherList = Arrays.asList(wmsArray);

            for(WMS s : otherList)
            {
                System.out.println("\t" + s);
            }
        }
        catch (FileNotFoundException e)
        {

        }
        catch (IOException e)
        {

        }
    }
}
