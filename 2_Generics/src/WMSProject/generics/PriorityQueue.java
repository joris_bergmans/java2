package WMSProject.generics;

import java.security.Key;
import java.util.*;

/**
 * PriorityQueue voor het toevoegen van items aan de list in een prioriteitsvolgorde
 * @author Joris Bergmans
 * @version 1.0.0
 */
public class PriorityQueue<T> implements FIFOQueue<T>
{
    private TreeMap<Integer, LinkedList<T>> myMap = new TreeMap<>(Comparator.reverseOrder());
    LinkedList<T> newList;
    /**
     * Voegt een item toe in de lijst indien de waarde uniek is.
     *
     * @param element
     * @param priority
     * @return
     */
    @Override
    public boolean enqueue(T element, int priority)
    {

           newList  = new LinkedList<>();
           newList.add(element);
           myMap.put(priority, newList);


       return true;
    }

    /**
     * Verwijdert item uit de list die als oudste was toegevoegd
     * @return
     */
    @Override
    public T dequeue()
    {
      return newList.remove();
    }

    /**
     * Zoekt een item in de lijst
     * @param element
     * @return
     */
    @Override
    public int search(T element)
    {
        int returnGetal = 0;
            if(myMap.values().contains(element)) {
                Object index = myMap.keySet().toArray()[0];
                returnGetal = myMap.get(index).indexOf(element);
            }
            return returnGetal;
    }

    /**
     * Geeft het aantal items in de lijst weer
     * @return
     */
    @Override
    public int getSize() {
        return myMap.size();
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<Integer, LinkedList<T>> entry : myMap.entrySet())
        {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }
        return sb.toString();
    }

   /* @Override
    public String toString()
    {

    }*/

}
