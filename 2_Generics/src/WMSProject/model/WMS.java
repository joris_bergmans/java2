package model;

import java.time.LocalDate;

/**
 * De basisklasse WMS waar al de basis getters en setters instaan
 */
public class WMS implements Comparable<WMS>
{
    private String medewerker;
    private String order;
    private String pallet;
    private String product;
    private int magazijnregio;
    private LocalDate productiedatum;
    private boolean pickingafgerond;
    private double gewicht;

    /**
     * Lege constructor voor dummy data
     */
    public WMS(){}

    /**
     * Gevulde constructors waar de data mee door komt
     * @param medewerker
     * @param order
     * @param pallet
     * @param product
     * @param magazijnregio
     * @param productiedatum
     * @param pickingafgerond
     * @param gewicht
     */
    public WMS(String medewerker, String order, String pallet, String product, int magazijnregio, LocalDate productiedatum, boolean pickingafgerond, double gewicht)
    {
        this.medewerker = medewerker;
        this.order = order;
        this.pallet = pallet;
        this.product = product;
        this.magazijnregio = magazijnregio;
        this.productiedatum = productiedatum;
        this.pickingafgerond = pickingafgerond;
        this.gewicht = gewicht;
    }

    /**
     * Attribuut medewerker
     * @return
     */
    public String getMedewerker() {
        return medewerker;
    }

    /**
     * Attribuut order
     * @return
     */
    public String getOrder() {
        return order;
    }

    /**
     * Attribuut pallet
     * @return
     */
    public String getPallet() {
        return pallet;
    }

    /**
     * attribuut product
     * @return
     */
    public String getProduct() {
        return product;
    }

    /**
     * Attribuut magazijnregio
     * @return
     */
    public int getMagazijnregio() {
        return magazijnregio;
    }

    /**
     * Attribuut productiedatum
     * @return
     */
    public LocalDate getProductiedatum() {
        return productiedatum;
    }

    /**
     * Attribuut pickingafgerond
     * @return
     */
    public boolean isPickingafgerond() {
        return pickingafgerond;
    }

    /**
     * Attribuut gewicht
     * @return
     */
    public double getGewicht() {
        return gewicht;
    }

    /**
     * geeft een foutmelding indien medewerker leeg is
     * @param medewerker
     */
    public void setMedewerker(String medewerker)
    {
        if(medewerker.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een medewerker zijn ingevuld!");
        }
        this.medewerker = medewerker;
    }

    /**
     * Geeft een foutmelding indien order leeg is
     * @param order
     */
    public void setOrder(String order)
    {
        if(order.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een order zijn ingevuld!");
        }
        this.order = order;
    }

    /**
     * Geeft een foutmelding indien pallet leeg is
     * @param pallet
     */
    public void setPallet(String pallet)
    {
        if(pallet.isEmpty())
        {
            throw new IllegalArgumentException("Pallet mag niet leeg zijn!");
        }
        this.pallet = pallet;
    }

    /**
     * Geeft een melding indien product leeg is
     * @param product
     */
    public void setProduct(String product)
    {
        if(product.isEmpty())
        {
            throw new IllegalArgumentException("Er moet een product zijn ingevuld!");
        }
        this.product = product;
    }

    /**
     * Geeft een foutmelding indien geen geldige nummer heeft
     * @param magazijnregio
     */
    public void setMagazijnregio(int magazijnregio)
    {
        if(magazijnregio < 0)
        {
            throw new IllegalArgumentException("Magzijnregio moet een geldige nummer hebben!");
        }
        this.magazijnregio = magazijnregio;
    }

    /**
     * Geeft een foufmelding indien productiedatum in de toekomst ligt
     * @param productiedatum
     */
    public void setProductiedatum(LocalDate productiedatum)
    {
        if(productiedatum.isAfter(LocalDate.now()))
        {
            throw new IllegalArgumentException("Productiedatum kan niet na vandaag zijn!");
        }
        this.productiedatum = productiedatum;
    }

    /**
     * Geeft een foutmelding indien pickingafgerond nog niet afgerond is
     * @param pickingafgerond
     */
    public void setPickingafgerond(boolean pickingafgerond)
    {
        if(pickingafgerond == false)
        {
            throw new IllegalArgumentException("De picking is nog niet afgerond, gelieve eerst alles in te vullen alvorens te verwerken!");
        }
        this.pickingafgerond = pickingafgerond;
    }

    /**
     * Geeft een foutmelding indien gewicht onder de 0 komt of gelijk staat aan 0
     * @param gewicht
     */
    public void setGewicht(double gewicht)
    {
        if(gewicht <= 0)
        {
            throw new IllegalArgumentException("Gewicht mag niet onder de 0 komen of gelijk zijn aan 0, gelieve een geldig gewicht in te vullen");
        }
        this.gewicht = gewicht;
    }

    @Override
    public int compareTo(WMS o)
    {
        return 0;
    }
}
